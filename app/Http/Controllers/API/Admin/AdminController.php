<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use App\Http\Resources\Admin\AdminCollection;
use App\Repositories\AdminRepository;

class AdminController extends Controller
{
    private $admin;

    public function __construct(AdminRepository $admin)
    {
        $this->admin = $admin;
    }
    
    public function index(Request $request)
    {
        return new AdminCollection($this->admin->paginate($request->perPage));
    }

    public function store(AdminRequest $request)
    {
        return $this->admin->store($request->all());
    }

    public function show($id)
    {
        return $this->admin->show($id);
    }

    public function update(AdminRequest $request, $id)
    {
        return $this->admin->update($request->all(), $id);
    }

    public function destroy($ids)
    {
        return $this->admin->destroy($ids);
    }
}
