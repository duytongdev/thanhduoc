<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Http\Resources\Admin\CategoryCollection;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }
    
    public function index(Request $request)
    {
        return new CategoryCollection($this->category->paginate($request->perPage));
    }

    public function store(CategoryRequest $request)
    {
        return $this->category->store($request->all());
    }

    public function show($id)
    {
        return $this->category->show($id);
    }

    public function update(CategoryRequest $request, $id)
    {
        return $this->category->update($request->all(), $id);
    }

    public function destroy($ids)
    {
        $this->category->destroy($ids);

        return response()->make(null, 204);
    }
}
