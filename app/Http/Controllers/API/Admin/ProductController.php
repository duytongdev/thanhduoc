<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Resources\Admin\ProductCollection;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }
    
    public function index(Request $request)
    {
        return new ProductCollection($this->product->paginate($request->perPage));
    }

    public function store(ProductRequest $request)
    {
        return $this->product->store($request->all());
    }

    public function show($id)
    {
        return $this->product->show($id);
    }

    public function update(ProductRequest $request, $id)
    {
        return $this->product->update($request->all(), $id);
    }

    public function destroy($ids)
    {
        $this->product->destroy($ids);

        return response()->make(null, 204);
    }
}
