<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as UserResource;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile(Request $request, $id)
    {
		return new UserResource(User::findOrFail(auth()->id()));
    }
}
