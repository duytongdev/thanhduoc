<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	$table = $request->table;
        $collectionNamespace = 'App\Http\Resources\Admin\\';

    	if (substr($table, -3)  === 'ies') {
            $model = '\App\\' . ucfirst(substr($table, 0, -3)) . 'y';
            $collection = $collectionNamespace . ucfirst(substr($table, 0, -3)) . 'y' . 'Collection';
        } else {
            $model = '\App\\' . ucfirst(substr($table, 0, -1));
            $collection = $collectionNamespace . ucfirst(substr($table, 0, -1)) . 'Collection';
        }

        $model = $model::where(function ($query) use ($request) {
            foreach ($request->columns as $column) {
                $query->orWhere($column, 'like', '%' . $request->keyword . '%');
            }
        });

    	return new $collection($model->paginate($request->perPage));
    }
}
