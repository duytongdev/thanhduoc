<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Resources\Admin\UserCollection;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        return new UserCollection($this->user->paginate($request->perPage));
    }

    public function store(UserRequest $request)
    {
        $this->user->store($request);

        return response()->make(null, 201);
    }

    public function show($id)
    {
        return $this->user->show($id);
    }

    public function update(UserRequest $request, $id)
    {
        return $this->user->update($request->all(), $id);
    }

    public function destroy($ids)
    {
        $this->user->destroy($ids);

        return response()->make(null, 204);
    }
}
