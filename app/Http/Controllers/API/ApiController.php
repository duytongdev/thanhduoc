<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="Mỹ Phẩm Thanh Dược API Documentation"
 * )
 */

/**
 * @OA\Server(
 *     url=L5_SWAGGER_CONST_HOST
 * )
 */

/**
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     description="Use a global client_id / client_secret and your username / password combo to obtain a token",
 *     name="Password Based",
 *     in="header",
 *     scheme="https",
 *     securityScheme="Password Based",
 *     @OA\Flow(
 *         flow="password",
 *         authorizationUrl="/oauth/authorize",
 *         tokenUrl="/oauth/token",
 *         refreshUrl="/oauth/token/refresh",
 *         scopes={}
 *     )
 * )
 */

/**
 * @OA\Get(
 *     path="/categories",
 *     operationId="getCategoriesList",
 *     tags={"Categories"},
 *     summary="Get list of categories",
 *     description="Returns list of categories",
 *     @OA\Response(
 *         response=200,
 *         description="successful operation"
 *     ),
 *     @OA\Response(response=400, description="Bad request"),
 *     security={
 *         {"api_key_security_example": {}}
 *     }
 * )
 *
 * Returns list of categories
 */

/**
 * @OA\Get(
 *      path="/categories/{id}",
 *      operationId="getCategoryById",
 *      tags={"Categories"},
 *      summary="Get category information",
 *      description="Returns category data",
 *      @OA\Parameter(
 *          name="id",
 *          description="Category id",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "oauth2_security_example": {"write:categories", "read:categories"}
 *         }
 *     },
 * )
 */

/**
 * @OA\Get(
 *     path="/products",
 *     operationId="getProductsList",
 *     tags={"Products"},
 *     summary="Get list of products",
 *     description="Returns list of products",
 *     @OA\Response(
 *         response=200,
 *         description="successful operation"
 *     ),
 *     @OA\Response(response=400, description="Bad request"),
 *     security={
 *         {"api_key_security_example": {}}
 *     }
 * )
 *
 * Returns list of products
 */

/**
 * @OA\Get(
 *      path="/products/{id}",
 *      operationId="getProductById",
 *      tags={"Products"},
 *      summary="Get product information",
 *      description="Returns product data",
 *      @OA\Parameter(
 *          name="id",
 *          description="Product id",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "oauth2_security_example": {"write:products", "read:products"}
 *         }
 *     },
 * )
 */
class ApiController extends Controller
{
    // 
}
