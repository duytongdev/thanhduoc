<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }



    /**
     * @OA\Post(
     *     path="/auth/login",
     *     tags={"Người dùng"},
     *     summary="Đăng nhập",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="username",
     *                     description="Mã đăng ký kinh doanh hoặc số điện thoại",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Mật khẩu",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Logged up successfully."
     *     ),
     *     @OA\Response(
     *         response=202,
     *         description="Unactivated account."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Login failed."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="The given data was invalid."
     *     )
     * )
     */
    public function login(LoginRequest $request)
    {
        $phoneNumber = $this->user->where('phone_number', $request->username)->first();

        $phoneNumber ? $username = 'phone_number' : $username = 'business_partner_code';

    	$credentials = [
            $username => $request->username,
            'password' => $request->password
        ];

    	if (!auth()->attempt($credentials)) {
            return response()->json(['message' => 'Thông tin tài khoản không tìm thấy trong hệ thống.'], 401);
    	}

    	$user = $request->user();

    	if ($user->activate === 0) {
    		return response()->json(['message' => 'Tài khoản chưa được kích hoạt.'], 202);
    	}

    	$tokenResult = $user->createToken('Personal Access Token');
    	$token = $tokenResult->token;

    	$token->save();

    	return response()->json([
    		'access_token' => $tokenResult->accessToken,
    		'token_type' => 'Bearer',
    		'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
    	]);
    }

    public function logout(Request $request)
    {
    	$request->user()->token()->revoke();

        return response()->make(null, 205);
    }
}
