<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserImage;
use Illuminate\Http\Request;
use Validator;

class SignupController extends Controller
{
    private $user;

    private $userImage;

    public function __construct(User $user, UserImage $userImage)
    {
        $this->user = $user;

        $this->userImage = $userImage;
    }

    /**
     * @OA\Post(
     *     path="/auth/signup",
     *     tags={"Người dùng"},
     *     summary="Đăng ký",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     description="Tên",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="date_of_birth",
     *                     description="Ngày sinh",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     description="Số điện thoại",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Mật khẩu",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     description="Địa chỉ",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="province",
     *                     description="Tỉnh",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="referrer",
     *                     description="Người giới thiệu",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="note",
     *                     description="Chú thích",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="images",
     *                     description="Hình ảnh",
     *                     type="file"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Signed up successfully."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="The given data was invalid."
     *     )
     * )
     */
    public function signup(Request $request)
    {
        $validator = $this->validateSignup($request);

        if ($validator->fails()) {
            return response()->json(['errors'  => $validator->errors()], 422);
        }

        $data = $request->except('images');

        $data['password'] = bcrypt($request->password);

    	$user = $this->user->create($data);

        foreach (config('phone_area_codes') as $province => $code) {
            if ($province === $user->province) {
                $businessPartnerCode = $code . $user->id;
            }
        }

        $this->user->findOrfail($user->id)->update(['business_partner_code' => $businessPartnerCode]);

        $this->storeImage($request->file('images'), $user->id);

        return response()->make(null, 201);
    }

    public function validateSignup($request)
    {
        return Validator::make($request->all(), [
            'name'          => 'required|string|max:255',
            'date_of_birth' => 'required|string|max:255',
            'phone_number'  => 'required|string|max:255|unique:users',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|string|min:6|max:255',
            'address'       => 'required|string|max:255',
            'province'      => 'required|string|max:255',
            'referrer'      => 'required|string|max:255',
            'images'        => 'required'
        ]);
    }

    public function storeImage($images, $userId)
    {
        $filePrefix = time() . '_' . str_random(10) . '.';

        if (is_array($images)) {
            $userImages = [];

            foreach ($images as $image) {
                $fileName = $filePrefix . $image->getClientOriginalExtension();

                $image->storeAs('users', $fileName);

                $timestamp = now();

                $userImages[] = [
                    'path' => 'users/' . $fileName,
                    'user_id' => $userId,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp
                ];
            }

            $this->userImage->insert($userImages);
        } else {
            $fileName = $filePrefix . $images->getClientOriginalExtension();

            $images->storeAs('users', $fileName);

            $this->userImage->create([
                'path' => 'users/' . $fileName,
                'user_id' => $userId
            ]);
        }
    }
}
