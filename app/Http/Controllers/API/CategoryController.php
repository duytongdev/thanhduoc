<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Repositories\CategoryRepository;

class CategoryController extends ApiController
{
	private $category;

	public function __construct(CategoryRepository $category)
	{
		$this->category = $category;
	}

    public function index()
    {
        return new CategoryCollection($this->category->paginate());
    }

    public function show($id)
    {
    	return new CategoryResource($this->category->show($id));
    }
}
