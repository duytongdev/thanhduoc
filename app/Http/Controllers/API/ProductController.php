<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Repositories\ProductRepository;

class ProductController extends ApiController
{
	private $product;

	public function __construct(ProductRepository $product)
	{
		$this->product = $product;
	}

    public function index()
    {
        return new ProductCollection($this->product->paginate());
    }

    public function show($id)
    {
    	return new ProductResource($this->product->show($id));
    }
}
