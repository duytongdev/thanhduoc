<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|max:255',
            'date_of_birth' => 'required|string|max:255',
            'phone_number'  => 'required|string|max:255|unique:users,phone_number' . $this->user,
            'email'         => 'required|string|email|unique:users,email' . $this->user,
            'password'      => 'required|string|min:6|max:255',
            'address'       => 'required|string|max:255',
            'province'      => 'required|string|max:255',
            'referrer'      => 'required|string|max:255',
            'images'         => 'required'
        ];
    }
}
