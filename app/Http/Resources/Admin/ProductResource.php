<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'price'       => $this->price,
            'image'       => $this->image,
            'description' => $this->description,
            'detail'      => $this->detail,
            'usage'       => $this->usage,
            'visibility'  => $this->visibility
        ];
    }
}
