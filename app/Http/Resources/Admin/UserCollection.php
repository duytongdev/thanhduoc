<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($item) {

            $item->activate === 0 ? $activate = 'Chưa kích hoạt' : $activate = 'Đã kích hoạt';

            return [
                'id' => $item->id,
                'name' => $item->name,
                'phone_number' => $item->phone_number,
                'email' => $item->email,
                'business_partner_code' => $item->business_partner_code,
                'activate' => $activate
            ];
        });
    }
}
