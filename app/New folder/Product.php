<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }
}
