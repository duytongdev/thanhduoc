<?php

namespace App\Repositories;

class CategoryRepository extends Repository
{
    public function model()
    {
    	return 'App\Category';
    }
}
