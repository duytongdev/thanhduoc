<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Storage;

class ProductRepository extends Repository
{
    public function model()
    {
    	return 'App\Product';
    }

    public function store($attributes)
    {
        $image = $this->handleUploadedImage($attributes);

        $product = $this->model->create([
            'name'        => $attributes['name'],
            'price'       => $attributes['price'],
            'image'       => $image,
            'description' => $attributes['description'],
            'detail'      => $attributes['detail'],
            'usage'       => $attributes['usage']
        ]);

        $product->categories()->sync($attributes['category_ids']);
    }

    public function handleUploadedImage($attributes)
    {
        $exploded = explode(',', $attributes['image']);
        $fileContents = base64_decode($exploded[1]);
        $fileName = $attributes['image_name'];
        $filePath = 'products/' . $fileName;

        Storage::disk('public')->put($filePath, $fileContents);

        return $filePath;
    }
}
