<?php

namespace App\Repositories;

use App\UserImage;
use Illuminate\Support\Facades\Storage;

class UserRepository extends Repository
{
    public function model()
    {
    	return 'App\User';
    }

    public function store($attributes)
    {
    	$data = $attributes->except('images');

    	$data['password'] = bcrypt($attributes->password);

        $user = $this->model->create($data);

        foreach (config('phone_area_codes') as $province => $code) {
        	if ($province === $user->province) {
        		$businessPartnerCode = $code . $user->id;
        	}
        }

        $this->model->findOrFail($user->id)->update(['business_partner_code' => $businessPartnerCode]);

        $this->storeImage($attributes->images, $user->id);
    }

    public function storeImage($images, $userId)
    {
    	$paths = [];

    	foreach ($images as $image) {
    		$path = 'users/' . $image['name'];
    		$timestamp = now();

    		Storage::disk('public')->put($path, base64_decode($image['content']));

    		$paths[] = [
    			'path' => $path,
    			'user_id' => $userId,
    			'created_at' => $timestamp,
    			'updated_at' => $timestamp
    		];
    	}

    	UserImage::insert($paths);
    }
}
