<?php

use App\Category;
use Faker\Factory;
use Goutte\Client;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $categories = [];

        foreach (range(0, 100) as $value) {
            $timestamp = now();

            $categories[] = [
                'name' => $faker->name,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
            ];
        }

        Category::insert($categories);

    	// $client = new Client();
    	// $crawler = $client->request('GET', 'http://myphamthanhduoc.com/danh-muc/cham-soc-co-the');
    	// $categories = $crawler->filter('.product-categories a')->each(function ($node) {
    	// 	return $node->text();
    	// });

    	// foreach ($categories as $category) {
    	// 	$categories = [];
    	// 	$timestamp = now();
    		
    	// 	$categories[] = [
    	// 		'name'       => $category,
    	// 		'created_at' => $timestamp,
    	// 		'updated_at' => $timestamp
    	// 	];

    	// 	Category::insert($categories);
    	// }
    }
}
