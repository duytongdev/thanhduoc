<?php

use App\Attribute;
use App\AttributeValue;
use App\Category;
use App\Product;
use Goutte\Client;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();

        $url = 'http://myphamthanhduoc.com/cua-hang-my-pham-dong-y-thanh-duoc';

        $crawler = $client->request('GET', $url);

        $selector = '.naturalife-wc-image-holder .woocommerce-LoopProduct-link';

        $links = $crawler->filter($selector)->each(function ($node) {
            return $node->attr('href');
        });

        $this->crawlData($links);
    }

    public function crawlData($links)
    {
        foreach ($links as $link) {
            $client = new Client();

            $crawler = $client->request('GET', $link);

            $name = $crawler->filter('h1')->each(function ($node) {
                return $node->text();
            });

            $price = $crawler->filter('.summary .amount')->each(function ($node) {
                return preg_replace('/[^0-9]/', '', $node->text());
            });

            $image = $crawler->filter('.woocommerce-product-gallery__image img')->each(function ($node) {
                return $node->attr('src');
            });

            $description =  $crawler->filter('.p1')->each(function ($node) {
                return $node->text();
            });

            $detail = $crawler->filter('#tab-description')->each(function ($node) {
                // Remove symbols except for right slash and the colon.
                $rawDetail = preg_replace('/[^\p{L}\p{N}\\\\:\s]/u', '', strip_tags($node->html()));

                return str_replace(["Mô tả\n", "\n "], ['', ''], $rawDetail);
            });

            $categories = $crawler->filter('.posted_in a')->each(function ($node) {
                return $node->text();
            });

            $attributes = $crawler->filter('.shop_attributes th')->each(function ($node) {
                return $node->text();
            });

            $attributeValues = $crawler->filter('.shop_attributes p')->each(function ($node) {
                return $node->text();
            });

            $data = array_column([$name, $price, $description, $detail, $image ], 0);

            $storagePath = $this->storeImage($data[4]);

            $product = $this->createProduct($data, $storagePath);

            $categoryIds = Category::whereIn('name', $categories)->pluck('id')->toArray();

            $product->categories()->sync($categoryIds);

            $attributeIds = $this->createAttribute($attributes, $product->id);

            $this->createAttributeValue($attributeIds, $attributeValues, $product->id);
        }
    }

    public function storeImage($link)
    {
        $fileName      = pathinfo($link, PATHINFO_FILENAME);
        $fileExtension = pathinfo($link, PATHINFO_EXTENSION);
        $storagePath   = 'products/' . $fileName . '.' . $fileExtension;
        $fileContents  = file_get_contents($link);

        Storage::disk('public')->put($storagePath, $fileContents);

        return $storagePath;
    }

    public function createProduct($data, $storagePath)
    {
        return Product::create([
            'name'              => $data[0],
            'price'             => $data[1],
            'short_description' => $data[2],
            'description'       => $data[3],
            'image'             => $storagePath
        ]);
    }

    public function createAttribute($attributes, $productId)
    {
        $attributeIds = [];

        foreach ($attributes as $name) {
            $attribute = Attribute::create([
                'name'       => $name,
                'product_id' => $productId
            ]);

            $attributeIds[] = $attribute->id;
        }

        return $attributeIds;
    }

    public function createAttributeValue($attributeIds, $attributeValues, $productId)
    {
        foreach (array_combine($attributeIds, $attributeValues) as $attributeId => $value) {
            AttributeValue::create([
                'value'        => $value,
                'attribute_id' => $attributeId,
                'product_id'   => $productId
            ]);
        }
    }
}
