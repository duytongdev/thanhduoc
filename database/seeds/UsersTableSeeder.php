<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = [];

        $password = bcrypt('secret');

        foreach (range(1, 100) as $index) {
        	$timestamp = now();

        	$users[] = [
        		'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => $password,
        		'date_of_birth' => $faker->date->format('d-m-Y'),
                'phone_number' => $faker->tollFreePhoneNumber,
                'address' => $faker->address,
                'province' => $faker->city,
                'referrer' => $faker->name,
                'image' => $faker->imageUrl,
                'created_at' => $timestamp,
                'updated_at' => $timestamp
        	];
        }

        App\User::insert($users);
    }
}
