import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VeeValidate, { Validator } from 'vee-validate'
import vi from 'vee-validate/dist/locale/vi'
import VueProgressBar from 'vue-progressbar'
import VueHeadFul from 'vue-headful'

Vue.config.productionTip = false

require('@/bootstrap')

Validator.localize('vi', vi)

Vue.use(VeeValidate)
Vue.use(VueProgressBar)

Vue.component('v-headful', VueHeadFul)
Vue.component('v-dropdown', require('./components/VDropdown'))
Vue.component('v-data-table', require('./components/VDataTable'))
Vue.component('v-tooltip', require('./components/VTooltip'))
Vue.component('v-form-group', require('./components/VFormGroup'))
Vue.component('v-form-label', require('./components/VFormLabel'))
Vue.component('v-form-control', require('./components/VFormControl'))
Vue.component('v-form-feedback', require('./components/VFormFeedback'))

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
