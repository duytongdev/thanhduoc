import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      component: require('@/views/NotFound')
    },
    {
      path: '/admin/login',
      component: require('@/views/Login'),
      name: 'login'
    },
    {
      path: '/admin',
      component: require('@/views/Admin'),
      children: [
        {
          path: 'dashboard',
          component: require('@/views/Dashboard'),
          name: 'dashboard'
        },
        {
          path: 'categories',
          component: require('@/views/Category'),
          children: [
            {
              path: '',
              component: require('@/views/CategoryList'),
              name: 'categories'
            },
            {
              path: 'create',
              component: require('@/views/CategoryCreate'),
              name: 'categories-create'
            },
            {
              path: 'edit/:id',
              component: require('@/views/CategoryEdit'),
              name: 'categories-edit'
            }
          ]
        },
        {
          path: 'products',
          component: require('@/views/Product'),
          children: [
            {
              path: '',
              component: require('@/views/ProductList'),
              name: 'products'
            },
            {
              path: 'create',
              component: require('@/views/ProductCreate'),
              name: 'products-create'
            },
            {
              path: 'edit/:id',
              component: require('@/views/ProductEdit'),
              name: 'products-edit'
            }
          ]
        },
        {
          path: 'users',
          component: require('@/views/User'),
          children: [
            {
              path: '',
              component: require('@/views/UserList'),
              name: 'users'
            },
            {
              path: 'create',
              component: require('@/views/UserCreate'),
              name: 'users-create'
            },
            {
              path: 'show/:id',
              component: require('@/views/UserShow'),
              name: 'users-show'
            },
            {
              path: 'edit/:id',
              component: require('@/views/UserEdit'),
              name: 'users-edit'
            }
          ]
        },
        {
          path: 'profile',
          component: require('@/views/Profile'),
          name: 'profile'
        }
      ]
    }
  ]
})
