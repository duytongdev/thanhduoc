<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- Only run this line if the server is HTTPS --}}
    {{-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">  --}}

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-grey-lighter font-sans text-sm text-grey-darker">
    <div id="app"></div>
</body>
</html>
