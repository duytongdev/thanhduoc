<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="{{ route('upload') }}" method="post" enctype="multipart/form-data">
		@csrf
		<input type="file" multiple="" name="images[]">
		<button type="submit">Upload</button>
	</form>
</body>
</html>