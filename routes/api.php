<?php

use Goutte\Client;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () {
	Route::get('categories', 'CategoryController@index');
	Route::get('categories/{id}', 'CategoryController@show');

	Route::get('products', 'ProductController@index');
	Route::get('products/{id}', 'ProductController@show');

	Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
		Route::post('signup', 'SignupController@signup');
		Route::post('login', 'LoginController@login');
		
		Route::group(['middleware' => 'auth:api'], function () {
			Route::post('logout', 'LoginController@logout');
		});
	});
});

Route::group(['prefix' => 'admin', 'namespace' => 'API\Admin'], function () {
	Route::apiResources([
		'admins' => 'AdminController',
		'categories' => 'CategoryController',
		'products' => 'ProductController',
		'users' => 'UserController',
		'orders' => 'OrderController'
	]);

	Route::get('search', 'SearchController@search');

	Route::get('province', function () {
		return response()->json(config('province'));
	});
});

Route::post('v1/auth/signup', 'API\Auth\SignupController@signup');
