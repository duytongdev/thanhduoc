<?php

use Goutte\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::group(['prefix' => '/admin'], function () {
	Route::get('{any}', function () {
		return view('index');
	})->where('any', '.*');

	Route::post('login', 'Auth\AdminLoginController@login');
});

Route::get('upload', function () {
	return view('upload');
});
Route::post('upload', 'API\Auth\SignupController@signup')->name('upload');
